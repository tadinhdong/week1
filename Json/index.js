
var das_data = get_data_json('Json/data.json')
var logs_data = get_data_json('Json/equipment.json')

// hide- show sider bar
body = document.getElementsByTagName("body")[0];
function collapseSidebar() {
    body = document.getElementsByTagName("body")[0];
    body.classList.toggle("sidebar-expand");
}

function get_data_json(file) {
    var items = []
    $.getJSON(file, function (data) {
        console.log("Processing data");
    })
        .done(function (data) {
            console.log("second success");
            $.each(data, function (key, val) {
                items.push(val)
            });
        })
        .fail(function () {
            console.log("error");
        });
    return items
}
function render_das() {
    $.each(das_data, function (key, val) {
        console.log(das_data);
        $("#list-items").append(
            "'<tr><td>" +
            val.devices +
            "</td><td>" +
            val.mac_address +
            "</td><td>" +
            val.ip +
            "</td><td>" +
            val.createdDate +
            "</td><td>" +
            val.powerConsumption +
            "</td></tr>"
        );
    });
}

//add
function add_device() {
    const d = new Date();
    var name = $("#name").val();
    var ip = $("#ip").val();

    if (name != "" && ip != "") {
        let device = {
            devices: name,
            mac_address: "00:1B:44:11:3A:B7",
            ip: ip,
            createdDate: d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay(),
            powerConsumption: Math.floor(Math.random() * 100 + 1),
        };
        das_data.push(device);
        $("#list-items").append(
            "'<tr><td>" +
            name +
            "</td><td>" +
            "00:1B:44:11:3A:B7" +
            "</td><td>" +
            ip +
            "</td><td>" +
            d.getFullYear() +
            "-" +
            d.getMonth() +
            "-" +
            d.getDay() +
            "</td><td>" +
            Math.floor(Math.random() * 100 + 1) +
            "</td></tr>"
        );
    } else {
        $("#password_error").text("Khong duoc de trong");
    }
    console.log(das_data);
    name = $("#name").val(null);
    ip = $("#ip").val(null);
    load_chart();
}
// chart
function load_chart() {
    const colors = [
        "#00aefd",
        "#ffa400",
        "#07a787",
        "#ff7870",
        "black",
        "pink",
        "yellow",
        "#e74c3c",
        "#2979f",
      ];
    var title = [];
    var param = [];
    var color = ["#F08080", "#FF7F50", "#e8c3b9", "#50C7C7"]
    $.each(das_data, function (key, val) {
        title.push(val.devices);
        param.push(val.powerConsumption);
        color.push(colors[Math.floor(Math.random() * colors.length)])
    });

    new Chart(document.getElementById("doughnut-chart"), {
        type: "doughnut",
        data: {
            labels: title,
            datasets: [
                {
                    label: "Population (millions)",
                    backgroundColor: color,
                    data: param,
                },
            ],
        },
        options: {
            title: {
                display: true,
                text: "Power Consumption(Kw/h)",
            },
        },
    });
}
