
$(document).ready(function () {
  $.each(logs_data, function (key, val) {
    console.log(val);
    $("#list-items").append(
      "'<tr><td>" +
      key +
      "</td><td>" +
      val.name +
      "</td><td>" +
      val.action +
      "</td><td>" +
      val.date +
      "</td></tr>"
    );
  });
  // so hang can hien thi
  var show_per_page = 12;
  //dem tat ca hang
  var number_of_items = $("#list-items").children().length;
  //chia lay so trang
  var number_of_pages = Math.ceil(number_of_items / show_per_page);
  // set trang hien tai
  $("#current_page").val(0);
  $("#show_per_page").val(show_per_page);

  var navigation_html =
    '<a class="previous_link" href="javascript:previous();">«</a>';

  var current_link = 0;
  while (number_of_pages > current_link) {
    navigation_html +=
      '<a class="page_link" href="javascript:go_to_page(' +
      current_link +
      ')" longdesc="' +
      current_link +
      '">' +
      (current_link + 1) +
      "</a>";
    current_link++;
  }

  navigation_html +=
    '<a class="next_link" href="javascript:next();">»</a>';

  $("#page_navigation").html(navigation_html);

  //active trang hien tai
  $("#page_navigation .page_link:first").addClass("active_page");

  //an hang k dung
  $("#list-items").children().css("display", "none");

  //hien hang cần dùng
  $("#list-items")
    .children()
    .slice(0, show_per_page)
    .css("display", "table-row");

  console.log(logs_data);
});

//get data

$(document).ready(function () {
  $("#myInput").on("keyup", function (event) {
    event.preventDefault();
    /* Act on the event */
    var key = $(this).val().toLowerCase();
    $("#list-items tr").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(key) > -1);
    });
  });
});

// previous page
function previous() {
  new_page = parseInt($('#current_page').val()) - 1;
  if ($('.active_page').prev('.page_link').length == true) {
    go_to_page(new_page);
  }
}
// next page
function next() {
  new_page = parseInt($('#current_page').val()) + 1;
  if ($('.active_page').next('.page_link').length == true) {
    go_to_page(new_page);
  }

}

//load page
function go_to_page(page_num) {
  var show_per_page = parseInt($('#show_per_page').val());

  start_from = page_num * show_per_page;

  end_on = start_from + show_per_page;

  $('#list-items').children().css('display', 'none').slice(start_from, end_on).css('display', 'table-row');

  $('.page_link[longdesc=' + page_num + ']').addClass('active_page').siblings('.active_page').removeClass('active_page');

  $('#current_page').val(page_num);
}
